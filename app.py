import os, math
from flask import Flask, request, render_template
from datetime import datetime
import requests

app = Flask(__name__)



@app.route('/')
def index():
    return 'Jenkins X1 test', 200

@app.route('/sentence')
def sentence():
    return 'Hello World, Jenkins version X', 200

@app.route('/hello')
def hello():
    return 'Hello World, Jenkins X version: %s' % (datetime.now())






if __name__ == "__main__":
    app.run(host='0.0.0.0', port=4000)
